#include <iostream>
#include <fstream>
#include <string.h>

int updateRepo()
{
    std::string repoTxt;
    std::string execute = "curl https://gitlab.com/tonn3k/experiment1/-/raw/master/packlist?ref_type=heads";
    const char *command = execute.c_str();
    char buffer[50];
    FILE *output = popen(command, "r");
    if (!output)
    {
        std::cout << "Error encountered when downloading repository";
        return 1;
    }

    try {
        while(fgets (buffer, sizeof(buffer), output)!=NULL)
            repoTxt+=buffer;
    } catch (...) {
        std::cout << "Error encountered when extracting repository";
        return 1;
    }

    //file handling time
    std::ofstream repo("/tmp/repositorycache.txt");
    repo << repoTxt;
    repo.close();
    pclose(output);
    return 0;
}

int searchRepo(char *package)
{
    std::string buffer;
    std::ifstream repo("/tmp/repositorycache.txt");
    if (!repo)
    {
        std::cout << "Repository not found, update the repository by mpkg update\n";
        return 1;
    }

    while (getline(repo, buffer))
    {
        int similarity=0;
        int z=0;
        for(int i=0;;i++) {
            if (buffer.size() == z)
                break;
            if (buffer[z]==package[i])
                similarity++;
            if (package[i]=='\0')
            {i=0; z++;}
        }

        //std::cout << "package similiarity: " << similarity << "\n"; // <- uncomment to test the index feature.

        if (similarity/strlen(package) >= 0.9)
        {
            //gain information like description, version
            std::string execute = "curl -s https://gitlab.com/tonn3k/experiment1/-/raw/master/packages/" + buffer + "?ref_type=heads";
            const char *command = execute.c_str();
            FILE * output = popen(command, "r");
            if (!output) {
                std::cout << "W: searching for a package named " + buffer + " is empty\n";
                continue;
            }
            char buffer2[300];
            std::string parameter;
            std::string description;
            std::string version;
            try {
                while (fgets(buffer2, sizeof(buffer2), output)!=NULL)
                {
                    parameter="";
                    for (int i=0; buffer2[i]!='\0'; i++)
                    {
                        parameter +=buffer2[i];
                        if (parameter == "version=") {
                            for (int x=i+1; buffer2[x]!='\0'; x++) {
                                if (buffer2[x]=='\n')
                                    break;
                                version+=buffer2[x];
                            }
                            break;
                        }
                        if (parameter == "description=") {
                            for (int x=i+1; buffer2[x]!='\0'; x++) {
                                if (buffer2[x]=='\n')
                                    break;
                                description+=buffer2[x];
                            }
                            break;
                        }
                    }
                }
                std::cout << "Package name: " + buffer + " | " + version + "\nDescription: " + description << std::endl;
            } catch (...) {
                std::cout << "Error encountered when reading package information";
                return 1;
            }
            pclose(output);
        }
    }
    repo.close();
    return 0;
}

int installPackage(char * package)
{
    std::string packagestr(package);
    std::string execute = "curl -s https://gitlab.com/tonn3k/experiment1/-/raw/master/packages/" + packagestr + "?ref_type=heads";
    const char *command = execute.c_str();
    FILE * output = popen(command, "r");
    if (!output)
    {
        std::cout << "Error encountered when getting information from package";
        return 1;
    }
    //now read packaging instructions
    bool source = false;
    std::string target;
    std::string parameter;
    char buffer[300];
    try {
        while (fgets(buffer, sizeof(buffer), output)!=NULL)
        {
            parameter="";
            for (int i=0; buffer[i]!='\0'; i++)
            {
                parameter+=buffer[i];
                if (parameter == "source=")
                {
                    for (int x=i+1; buffer[x]!='\0'; x++) {
                        if (buffer[x]=='\n')
                            break;
                        target+=buffer[x];
                    }
                    source=true;
                    break;
                }
                if (parameter == "download=")
                {
                    for (int x=i+1; buffer[x]!='\0'; x++) {
                        if (buffer[x]=='\n')
                            break;
                        target+=buffer[x];
                    }
                    break;
                }
            }
        }
    } catch (...) {
        std::cout << "Error encountered when reading package information";
        return 1;
    }

    pclose(output);

    //download (and compile)

    std::string execute2;

    if (source)
        execute2 = "curl -s -o build.c " + target;
    else
        execute2 = "curl -s -o binaryfile " + target;
    const char * command2 = execute2.c_str();
    output = popen(command2, "r");

    if (!output)
    {
        std::cout << "Error encountered when downloading package";
        return 1;
    }

    if (source)
    {
        system("gcc -O2 -march=native -pipe build.c");
        std::string execute3 = "su -c \"mv a.out /bin/\"" + packagestr;
        system(execute3.c_str());
        system("rm build.c");

    } else {
        system("chmod +x binaryfile");// <- it can't detect the binaryfile, assuming the su -c took the operation earlier than chmod?
        std::string execute3 = "su -c \"mv binaryfile /bin/\"" + packagestr;
        system(execute3.c_str());
    }
    //kinda easy.
    pclose(output);
    return 0;
}

int removePackage(char * package)
{
    std::string packagestr(package);
    std::ifstream repo("/tmp/repositorycache.txt");

    if (!repo)
    {
        std::cout << "Repository not found, update the repository by mpkg update\n";
        return 1;
    }

    std::string buffer;
    bool jobDone = false;

    while (getline(repo,buffer))
    {
        if (buffer==package)
        {
            std::string execute = "su -c \"rm /bin/\"" + packagestr;
            system(execute.c_str());
            std::cout << "Package removed" << std::endl;
            jobDone=true;
            break;
        }
    }

    if (!jobDone)
    {
        std::cout << "Package unknown" << std::endl;
        return 1;
    }

    return 0;
}

int main(int argc, char *args[])
{
    std::cout << "Package manager rewrite :(, hope it works" << std::endl;

    if (argc < 2)
    {
        std::cout << "Usage: mpkg [ACTION]\nACTIONS:\nupdate <- update repository\nsearch <- search package\nadd <- install a package\ndel <- uninstall a package\n";
        return 1;
    }

    if (strcmp(args[1], "update")==0)
        updateRepo();

    if (strcmp(args[1], "search")==0)
    {
        if (!args[2])
        {
            std::cout << "Please specify what packages to search\n"; return 1;
        }
        searchRepo(args[2]);
    }

    if (strcmp(args[1], "install")==0)
    {
        if (!args[2])
        {
            std::cout << "Please specify what packages to install\n"; return 1;
        }
        installPackage(args[2]);
    }

    if (strcmp(args[1], "remove")==0)
    {
        if (!args[2])
        {
            std::cout << "Please specify what packages to remove\n"; return 1;
        }
        removePackage(args[2]);
    }

    return 0;
}
